package static

import _ "embed"

//go:embed index.js
var JS []byte

//go:embed index.html
var HTML []byte

//go:embed lineup.json
var JSON []byte
