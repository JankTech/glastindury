console.log("hello world")

async function getLineup() {
  const res = await fetch("/lineup.json");
  const lineup = await res.json();
  const d = new Date(lineup[0].start);
  console.log(d);
}

getLineup();
