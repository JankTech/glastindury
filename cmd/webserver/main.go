package main

import (
	"log"
	"net/http"

	"github.com/NYTimes/gziphandler"
	"gitlab.com/JankTech/glastindury/static"
)

func main() {
	mux := http.NewServeMux()

	mux.Handle("GET /", gziphandler.GzipHandler(serveFile(static.HTML, "text/html")))
	mux.Handle("GET /index.js", gziphandler.GzipHandler(serveFile(static.JS, "application/javascript")))
	mux.Handle("GET /lineup.json", gziphandler.GzipHandler(serveFile(static.JSON, "text/json")))

	log.Fatal(http.ListenAndServe(":8080", mux))
}

func serveFile(file []byte, contentType string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", contentType)
		_, err := w.Write(file)
		if err != nil {
			panic(err)
		}
	}
}
