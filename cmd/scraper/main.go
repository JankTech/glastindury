package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gocolly/colly"
)

type Event struct {
	Name  string    `json:"name"`
	Start time.Time `json:"start"`
	End   time.Time `json:"end"`
	Stage string    `json:"stage"`
	URL   string    `json:"url"`
}

var quietMode *bool

func main() {
	shouldDownload := flag.Bool("download", false, "redownload the lineup page into tmp")
	htmlFilename := flag.String("html-filename", "tmp/lineup.html", "the filename of the html file downloaded from glastonbury website")
	jsonFilename := flag.String("json-filename", "tmp/lineup.json", "the filename of the json file output for the scraped output")
	quietMode = flag.Bool("quiet", false, "suppress most logs")

	flag.Parse()

	_, err := os.Stat(*htmlFilename)
	if *shouldDownload || errors.Is(err, os.ErrNotExist) {
		downloadPage(*htmlFilename)
	}

	scrapePage(*htmlFilename, *jsonFilename)
}

func scrapePage(htmlFilename string, jsonFilename string) {
	eventData := []Event{}

	transport := &http.Transport{}
	transport.RegisterProtocol("file", http.NewFileTransport(http.Dir("./")))

	collector := colly.NewCollector()
	collector.WithTransport(transport)

	collector.OnHTML("a[name] + div.box_header", func(e *colly.HTMLElement) {
		stage := e.Text
		lineupGroup := e.DOM.Next()
		lineupDays := lineupGroup.Find("table.lineup_table_day > tbody")
		lineupDays.Each(func(i int, dayTable *goquery.Selection) {
			day := ""
			lines := dayTable.Find("tr")
			lines.Each(func(i int, line *goquery.Selection) {
				if i == 0 {
					day = line.Text()
					return
				}

				if !*quietMode {
					log.Println("parsing: " + stage + " " + day + line.Text())
				}

				artist := ""
				url := ""
				a := line.Find("td > a")
				if a.Length() != 1 {
					artist = line.Find("td").First().Text()
				} else {
					artist = a.Text()
					var ok bool
					url, ok = a.Attr("href")
					if !ok {
						log.Fatalf("error: could not get artist URL for %s", artist)
					}
				}
				if artist == "OPEN" || artist == "TBA" || artist == "INTERVAL" {
					return
				}

				times := line.ChildrenFiltered("td + td")
				if times.Length() != 1 {
					log.Fatalf("error: unexpected number of time 'td's found. Could not parse. Stage = %s, day = %s, got %d expected 1\n", stage, day, times.Length())
				}

				tr, err := parseTime(day, times.Text())
				if err != nil {
					log.Fatalf("error: could not parse time: %s", err)
				}

				eventData = append(eventData, Event{
					Name:  artist,
					Start: tr.Start,
					End:   tr.End,
					Stage: stage,
					URL:   url,
				})
			})
		})
	})

	err := collector.Visit(fmt.Sprintf("file:///%s", htmlFilename))
	if err != nil {
		log.Fatalf("error: could not read file '%s': %s", htmlFilename, err)
	}

	file, err := os.Create(jsonFilename)
	if err != nil {
		log.Fatalf("error: failed to create or open json file '%s': %s\n", jsonFilename, err)
	}
	defer file.Close()

	err = json.NewEncoder(file).Encode(eventData)
	if err != nil {
		log.Fatalf("error: failed to write json data to file '%s': %s\n", jsonFilename, err)
	}

	log.Printf("json data written to file: %s\n", jsonFilename)
}

type timeRange struct {
	Start time.Time
	End   time.Time
}

var dates = map[string]string{
	"WEDNESDAY": "2024-06-26",
	"THURSDAY":  "2024-06-27",
	"FRIDAY":    "2024-06-28",
	"SATURDAY":  "2024-06-29",
	"SUNDAY":    "2024-06-30",
}

func parseTime(day string, timeRangeStr string) (timeRange, error) {
	var err error
	t := timeRange{}

	date, ok := dates[day]
	if !ok {
		return t, fmt.Errorf("could not parse day %s", day)
	}

	times := strings.Split(timeRangeStr, " - ")
	if len(times) != 2 {
		return t, fmt.Errorf("could not split time string %s", timeRangeStr)
	}

	startStr := date + "T" + times[0] + "+0100"
	t.Start, err = time.Parse("2006-01-02T15:04-0700", startStr)
	if err != nil {
		return timeRange{}, fmt.Errorf("could not parse time string %s: %w", startStr, err)
	}

	endStr := date + "T" + times[1] + "+0100"
	t.End, err = time.Parse("2006-01-02T15:04-0700", endStr)
	if err != nil {
		return timeRange{}, fmt.Errorf("could not parse time string %s: %w", endStr, err)
	}

	return t, nil
}

func downloadPage(filename string) {
	log.Println("downloading lineup html")
	const requestURL = `https://www.glastonburyfestivals.co.uk/line-up/line-up-2024/`

	log.Printf("requesting data from '%s'", requestURL)
	res, err := http.Get(requestURL)
	if err != nil {
		log.Fatalf("error: failed making http request: %s\n", err)
	}

	log.Printf("request completed with status code: %s.\n", res.Status)
	data, err := io.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("error: failed to read data from request body: %s\n", err)
	}

	err = os.WriteFile(filename, data, 0666)
	if err != nil {
		log.Fatalf("error: failed to write html data to file: %s\n", err)
	}
	log.Printf("html data written to file: %s\n", filename)
}
